import { Router, Route, Switch } from 'react-router-dom';
import Registration from './components/Registration';

import NavBar from './components/templates/NavBar';
import AdminReport from './components/AdminReport';
import Confirmation from './components/Confirmation';

import history from './history';
import NotFound from './components/templates/NotFound';

function App() {
  return (
    <div>
      <Router history={history}>
        <NavBar />
        <div style={{ margin: '7em' }}>
          <Switch>
            <Route path="/" exact component={Registration} />
            <Route
              path="/registration/success"
              exact
              component={() => <Confirmation successful />}
            />
            <Route
              path="/registration/failure"
              exact
              component={() => <Confirmation />}
            />
            <Route path="/admin/report" exact component={AdminReport} />
            <Route path="*" exact component={NotFound} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
