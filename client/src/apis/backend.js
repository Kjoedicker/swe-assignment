import axios from 'axios';

const API_URLS = {
  local: {
    API_URL: 'http://localhost:8080',
  },
  development: {
    API_URL: 'https://kjnix.com',
  },
};

export default axios.create({
  baseURL: API_URLS.development.API_URL,
});
