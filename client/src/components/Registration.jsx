import React from 'react';

import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { registerUser } from '../redux/actions';
import PageHeader from './templates/PageHeader';

const renderError = ({ error, touched }) => {
  if (touched && error) {
    return (
      <div className="ui error message">
        <div className="header">{error}</div>
      </div>
    );
  }
};

const renderInput = ({ input, label, meta }) => {
  const className = `field ${meta.error && meta.touched ? 'error' : ''}`;

  return (
    <div className={className}>
      <label>{label}</label>
      <input {...input} />
      <div>{renderError(meta)}</div>
    </div>
  );
};

const validate = (formValues) => {
  const errors = {};

  if (!formValues.first_name) {
    errors.first_name = 'First name required';
  }

  if (!formValues.last_name) {
    errors.last_name = 'Last name required';
  }

  if (!formValues.address_1 && !formValues.address_2) {
    errors.address_1 = 'Address required';
  }

  if (!formValues.city) {
    errors.city = 'City required';
  }

  if (!formValues.state) {
    errors.state = 'State required';
  }

  if (formValues.zip) {
    if (formValues.zip.length > 9 || formValues.zip.length < 2) {
      errors.zip = 'Must be 2 - 9 integers';
    }

    const nonIntegers = formValues.zip.match('[^-0-9\/]+')
    if (nonIntegers) {
      errors.zip = 'Must contain integers only'
    }

  } else {
    errors.zip = 'Zipcode required';
  }

  if (!formValues.country) {
    errors.country = 'Country required';
  } else if (
    !['united states', 'usa', 'us'].includes(formValues.country.toLowerCase())
  ) {
    errors.country = 'US residents only';
  }

  return errors;
};

function Registration(props) {
  const onSubmit = (formValues) => {
    props.registerUser(formValues);
  };

  return (
    <form
      onSubmit={props.handleSubmit(onSubmit)}
      className="ui form error container"
    >
      <PageHeader
        pageName="Registration"
        iconType="settings"
        subHeader="User Form"
      />

      <div className="ui segment form">
        <div className="ui two column very relaxed grid">
          <div className="column">
            <Field
              name="first_name"
              component={renderInput}
              label="Enter First Name"
            />
          </div>
          <div className="column">
            <Field
              name="last_name"
              component={renderInput}
              label="Enter Last Name"
            />
          </div>
        </div>

        <div className="ui two column very relaxed grid">
          <div className="column">
            <Field
              name="address_1"
              component={renderInput}
              label="Enter Address 1"
            />
          </div>
          <div className="column">
            <Field
              name="address_2"
              component={renderInput}
              label="Enter Address 2 (optional)"
            />
          </div>
        </div>

        <br />
        <div className="three fields">
          <Field name="city" component={renderInput} label="Enter City" />
          <Field name="state" component={renderInput} label="Enter State" />
          <Field name="country" component={renderInput} label="Enter Country" />
        </div>
        <div className="one fields">
          <Field name="zip" component={renderInput} label="Enter Zipcode" />
        </div>
        <br />
      </div>

      <button className="ui button primary">Submit</button>
    </form>
  );
}

const formWrapped = reduxForm({
  form: 'userRegistration',
  validate,
})(Registration);

export default connect(null, { registerUser })(formWrapped);
