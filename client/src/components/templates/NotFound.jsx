import React from 'react';

import history from '../../history';
import notFoundPicture from '../../images/notfound.png';

export default function NotFound() {
  return (
    <div>
      <div>
        <div className="ui raised card centered">
          <div className="image">
            <img src={notFoundPicture} />
          </div>
          <div className="content center aligned">
            <h1>Wrong Turn</h1>
            <i className="redo icon" onClick={() => history.push('/')} />
          </div>
        </div>
      </div>
    </div>
  );
}
