import React from 'react';

export default function PageHeader(props) {
  const { iconType, pageName, subHeader } = props;

  return (
    <h2 className="ui  header">
      <i className={`${iconType} icon`} />
      <div className="content">
        {pageName}
        <div className="sub header">{subHeader}</div>
      </div>
    </h2>
  );
}
