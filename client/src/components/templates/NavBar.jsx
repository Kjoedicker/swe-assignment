import React from 'react';
import { Link, useLocation } from 'react-router-dom';

const isActive = (path, currentPath) =>
  path === currentPath ? 'active' : null;

export default function NavBar(props) {
  const { pathname } = useLocation();

  return (
    <div className="ui inverted fixed menu main">
      <Link to="/" className={`item ${isActive('/', pathname)}`}>
        Registration
      </Link>

      <Link
        to="/admin/report"
        className={`item ${isActive('/admin/report', pathname)}`}
      >
        Admin
      </Link>

      <div className="right menu">
        <a className="ui item icon">
          <i className="ui avatar globe icon" />
        </a>
      </div>
    </div>
  );
}
