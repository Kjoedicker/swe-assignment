import React from 'react';

export default function TableRow(props) {
  const {
    index,
    first_name,
    last_name,
    address_1,
    address_2,
    city,
    state,
    country,
    zip,
    date,
  } = props;

  return (
    <tr key={index}>
      <td>{index}</td>
      <td>{`${first_name} ${last_name}`}</td>
      <td>{address_1 || '-'}</td>
      <td>{address_2 || '-'}</td>
      <td>{city}</td>
      <td>{state}</td>
      <td>{country}</td>
      <td>{zip}</td>
      <td>{date}</td>
    </tr>
  );
}
