import React from 'react';

import success from '../images/success.png';
import failure from '../images/failure.png';

import history from '../history';

export default function Confirmation(props) {
  const { successful } = props;

  return (
    <div>
      <div className="ui raised card centered">
        <div className="image">
          <img src={successful ? success : failure} />
        </div>
        <div className="content center aligned">
          <h1>{successful ? 'Success' : 'Failure'}</h1>
          <i className="redo icon" onClick={() => history.push('/')} />
        </div>
      </div>
    </div>
  );
}
