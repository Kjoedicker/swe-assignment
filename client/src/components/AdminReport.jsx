import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { fetchUsers } from '../redux/actions';

import TableRow from './templates/TableRow';
import PageHeader from './templates/PageHeader';

const mapUsers = (users) =>
  users.map((user, index) => {
    const {
      first_name,
      last_name,
      address_1,
      address_2,
      city,
      state,
      country,
      zip,
      createdAt: date,
    } = user;

    return (
      <TableRow
        index={index}
        first_name={first_name}
        last_name={last_name}
        address_1={address_1}
        address_2={address_2}
        city={city}
        state={state}
        country={country}
        zip={zip}
        date={date}
      />
    );
  });

function AdminReport(props) {
  useEffect(() => {
    props.fetchUsers();
  }, []);

  return (
    <div className="ui container">
      <PageHeader
        pageName="User Report"
        iconType="user"
        subHeader="Registered Users"
      />

      <table className="ui fixed black striped table">
        <thead>
          <tr>
            <th>Index</th>
            <th>Name</th>
            <th>Address 1</th>
            <th>Address 2</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th>ZIP</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>{mapUsers(props.users)}</tbody>
      </table>
      <br />
    </div>
  );
}

const mapStateToProps = (state) => ({
  users: Object.values(state.users),
});

export default connect(mapStateToProps, { fetchUsers })(AdminReport);
