import { REGISTER_USER, FETCH_USERS } from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case REGISTER_USER:
      return { ...state, ...action.payload };
    case FETCH_USERS:
      return { ...action.payload };
    default:
      return state;
  }
};
