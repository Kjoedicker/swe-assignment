import { FETCH_USERS, REGISTER_USER } from './types';

import history from '../../history';

import API from '../../apis/backend';

export const registerUser = (formValues) => async (dispatch) => {
  try {
    const response = await API.post('/registration', { ...formValues });

    dispatch({ type: REGISTER_USER, payload: response.data });
    history.push('/registration/success');
  } catch {
    history.push('/registration/failure');
  }
};

export const fetchUsers = () => async (dispatch) => {
  const response = await API.get('/admin/users');

  dispatch({ type: FETCH_USERS, payload: response.data.users });
};
