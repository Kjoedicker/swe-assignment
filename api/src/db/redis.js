// Core redis functionality
const redis = require('redis');
const client = redis.createClient({
    port: 6379,
    host: 'redis'
});

const util = require('util')
client.get = util.promisify(client.get)


module.exports = client;