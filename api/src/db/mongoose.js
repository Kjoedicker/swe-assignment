const mongoose = require('mongoose');

const concatConnectionString = () => {
  if (!process.env.LINK || !process.env.DB) {
    throw new Error(`Missing core mongoose configuraton in .env`)
  }

  return `mongodb://${process.env.LINK}:27017/${process.env.DB}`
}

const dbConnect = async () => {
  try {

    const connectionString = concatConnectionString();

    const connect = await mongoose.connect(`mongodb://${process.env.LINK}:27017/${process.env.DB}`, {
      useNewUrlParser: true,
      useCreateIndex: true,
    });

    console.log(`Successfully connected to ${connectionString}` )
  }catch (err) {
    console.log('Failed to connect to db\n', err.message)
  }
}

module.exports = dbConnect