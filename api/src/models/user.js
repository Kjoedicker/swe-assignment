const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
  first_name: {
    type: String, 
    require: true, 
    trim: true
  },
  last_name: {
    type: String, 
    required: true, 
    trim: true
  },
  address_1: {
    type: String,
    trim: true,
  },
  address_2: {
    type: String,
    trim: true,
  },
  city: {
    type: String,
    required: true,
    trim: true,
  },
  state: {
    type: String,
    required: true,
    trim: true,
  },
  zip: {
    type: String,
    required: true,
    trim: true,
  },
  country: {
    type: String,
    trim: true,
    required: true,
    default: 'US'
  }
}, { timestamps: true });


userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  userObject.createdAt = moment(userObject.createdAt).format("YYYY-MM-DD HH:mm:ss")

  delete userObject._id;
  delete userObject.__v;
  delete userObject.updatedAt;

  return userObject;
};

const User = mongoose.model('User', userSchema);

module.exports = User;