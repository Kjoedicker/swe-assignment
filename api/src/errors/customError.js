
class CustomError extends Error {
  constructor(code, message) {
    super(JSON.stringify(message));
    this.statusCode = code;
    Object.setPrototypeOf(this, CustomError.prototype);
  }

  serializeErrors() {
    return JSON.parse(this.message);
  }
}

module.exports = CustomError;

