const express = require('express');
const app = express();

const cors = require('cors');

require('dotenv').config('.env' );

// cross origin requests
app.use(cors())

// body parser
app.use(express.json({extended: false}));

// Imported routes
const RegistrationRoutes = require('./routes/registration');
const AdminRoutes = require('./routes/admin')

// In use routes
app.use(RegistrationRoutes);
app.use(AdminRoutes);

// // 404 route
app.get('*', function(req, res) {
  res.status(404).json({
    status: 'Failure',
    error: 'Invalid route',
  });
});

// Error handling 
const errorHandler = require('./middleware/errorHandler');
app.use(errorHandler);

module.exports = {app};
