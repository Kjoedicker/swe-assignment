const {app} = require('./index');

// connect to mongoose
const dbConnect = require('./db/mongoose');
dbConnect()

const {PORT}= process.env;
app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});

