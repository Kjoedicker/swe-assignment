const express = require('express');
const router = express.Router();
const {check, oneOf, validationResult} = require('express-validator');

const CustomError = require('../errors/customError')

const userCtrl = require('../controllers/userCtrl');

// @route POST /registration
// @desc Register a user
router.post('/registration',  
[
    check('first_name', 'A first name is required').notEmpty(),
    check('last_name', 'A last name is required').notEmpty(),
    oneOf(
      [check('address_1').notEmpty(), check('address_2').notEmpty()],
      'A valid address is required'
    ),
    check('city', 'A city is required').notEmpty(),
    check('state', 'A valid state is required').isLength({ min: 2 }),
    check('country', 'A valid country is required').toLowerCase().isIn([
      'us',
      'usa',
      'united states',
    ]),
    check('zip', 'A valid zip code is required').isLength({ min: 5, max: 9 }).isNumeric()
],
async (req, res, next) => {  
    let {errors} = validationResult(req);

    if (errors.length) {
        return next(new CustomError(422,  errors))
    }

    try {
        const newUser = await userCtrl.register(req, res, next);

        res.status(201).json({status: 'Success', newUser});               
    } catch (err) {
        next(err)
    }
});

module.exports = router;
