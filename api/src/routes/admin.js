const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/userCtrl');

// @route GET /admin/users
// @desc Get all user records
router.get('/admin/users', async (req, res, next) => {
    try {
        const users = await userCtrl.getAll(req, res, next);

        res.status(200).json({status: 'Success', users})
    } catch (err) {
        next(err)
    }
});

module.exports = router;
