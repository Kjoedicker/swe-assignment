const Users = require('../models/user');

const CustomError = require('../errors/customError')

const redisClient = require('../db/redis');

module.exports = {
    register: async(req)=> {

        const {first_name, last_name, address_1, address_2, city, state, zip, country} = req.body;

        const newUser = new Users({
            first_name,
            last_name,
            address_1,
            address_2, 
            city, 
            state, 
            zip, 
            country
        })
        
        const user = await newUser.save();
        await redisClient.flushdb()
    
        return user;
    },
    getAll: async ()=>{
        let users = await redisClient.get('users');

         if (!users) {
            users = await Users.find({}).sort({createdAt: -1});

            if (!users) {
                throw new CustomError(404, 'Users not found')
            }

            redisClient.set('users', JSON.stringify(users));
         } else {
             users = JSON.parse(users);
         }

        return users;
    }
}